FROM docker.io/docker:27.4.1

COPY ./requirements.txt ./requirements.txt

RUN <<EOF
apk add --no-cache python3 python3-dev py3-pip py3-cryptography build-base git oniguruma-dev
python3 -m pip install --break-system-packages -r ./requirements.txt
rm ./requirements.txt
EOF
